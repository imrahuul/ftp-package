<?php

/*
 * The sofware is provided under Mit License.
 * For the full copyright and license information, please view the LICENSE file
 */

namespace CodeClouds\FtpClient;

use CodeClouds\FtpClient\Interfaces\ServerInterface;
use CodeClouds\FtpClient\Interfaces\FactoryInterface;
use CodeClouds\FtpClient\Servers\FtpServer;
use CodeClouds\FtpClient\Servers\SftpServer;
use CodeClouds\FtpClient\Files\FtpFile;
use CodeClouds\FtpClient\Files\SftpFile;

/**
 * Factory for File classes
 *
 * @author CodeClouds <CodeClouds@gmail.com>
 * @package FtpClient
 */
class FileFactory implements FactoryInterface
{
    
    /**
     * Build method for File classes
     */
    public static function build(ServerInterface $server)
    {
        if ($server instanceof SftpServer) {
            return new SftpFile($server);
        } elseif ($server instanceof FtpServer || $server instanceof SslServer) {
            return new FtpFile($server);
        } else {
            throw new \InvalidArgumentException('The argument is must instance of server class');
        }
    }
    
}
